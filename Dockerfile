FROM kyma/docker-nginx
COPY app/ /var/www
EXPOSE 80/tcp
CMD 'nginx'