export default {
  genereteGUID: function() {
    return (
      this.generateGUIDPart() +
      this.generateGUIDPart() +
      "-" +
      this.generateGUIDPart() +
      "-" +
      this.generateGUIDPart() +
      "-" +
      this.generateGUIDPart() +
      "-" +
      this.generateGUIDPart() +
      this.generateGUIDPart() +
      this.generateGUIDPart()
    );
  },
  generateGUIDPart: function() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
};
