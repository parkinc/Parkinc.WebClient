export default {
  formatTime: function(hour, minute) {

    if(hour < 10){
        hour = '0' + hour;
    }

    if(minute < 10){
        minute = '0' + minute;
    }

    return hour + ':' + minute;
  },
  getCurrent: function(){

    var date = new Date();
    var hour = date.getHours();
    var minute = date.getMinutes();

    return this.formatTime(hour, minute);
  },
  getHourFromString:function(timeString){

    return timeString.split(":")[0];
  },
  getMinuteFromString: function(timeString){
    return timeString.split(":")[1];
  }
};
