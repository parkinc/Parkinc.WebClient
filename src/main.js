import Vue from 'vue'
import Vuetify from 'vuetify'
import './stylus/main.styl'
import App from './App'
import router from './router'
import { store } from './store'
import SocketIo from 'socket.io-client';
import VueSocketIo from 'vue-socket.io';

import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(Vuetify)
Vue.use(VueGoogleMaps, {
  load:{
    key: 'AIzaSyBvWE_sIwKbWkiuJQOf8gSk9qzpO96fhfY',
    libraries: 'places'
  }
})
Vue.config.productionTip = false

const socket = SocketIo("http://ws.parkinc.valorl.com", { transports: ['websocket'] });
Vue.use(VueSocketIo, socket, store);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
}) 

