import Vue from "vue";
import Router from "vue-router";

import Home from "@/components/Home";
import Parking from "@/components/Parking/Parking";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      redirect: "/home",
      name: "Home",
      component: Home
    },
    {
      path: "/home",
      name: "Home",
      component: Home
    },
    {
      path: "/parking",
      name: "Parking",
      component: Parking
    },
    {
      path: "*",
      redirect: "/home",
      name: "Home",
      component: Home
    }
  ],
  //mode: "history"
});
