import Axios from "axios";

export default {
  state: {
    source: ""
  },
  mutations: {
    setSource(state, payload) {
      state.source = payload;
    }
  },
  actions: {
    loadAdSource({commit}) {
      var url = "http://api.parkinc.valorl.com/ads/ad";
      Axios.get(url).then(response => {
        if(response == null || response == undefined || response == '' || response.data == '') return;
        
        let prefix = 'data:image/png;base64,';
        let source =  prefix + response.data;

        commit("setSource", source);
      });
    },
    closeAd({commit, dispatch}){
      commit("setSource", "");

      setTimeout(function(){ 
        dispatch("loadAdSource");
      }, 5000);
    }
  }, 
  getters: {
    source(state) {
      return state.source;
    }
  }
};
