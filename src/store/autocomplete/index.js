export default {
  state: {
    autocomplete: []
  },
  mutations: {
    setAutocomplete(state, payload) {
      state.autocomplete = payload;
    },
    pushToAutomplete(state, payload) {
      state.autocomplete.push(payload);
    }
  },
  actions: {
    findCities({ commit, dispatch }, payload) {
      commit("setAutocomplete", []);
      if (payload.length < 3) return;

      var jsonp = require("jsonp");
      var url = "http://gd.geobytes.com/AutoCompleteCity?q=" + payload;

      jsonp(url, null, function(err, data) {
        if (err) {
          console.error(err.message);
          return;
        }

        dispatch("showCities", data);
      });
    },
    showCities({ commit, getters }, payload) {
      if (payload == null || payload == undefined || payload.length <= 0)
        return;

      payload.forEach(function(dataRow, index) {
        
        if (dataRow == null || dataRow == "" || dataRow == undefined) {
          return;
        }

        var dataArray = dataRow.split(",");
        var city = dataArray[0].trim();
        var regionCode = dataArray[1].trim();
        var county = dataArray[2].trim();

        var payload = {
          id: index,
          city: city,
          region: regionCode,
          country: county,
          orderNumber: index + 1
        };

        commit("pushToAutomplete", payload);
      }, this);
    }
  },
  getters: {
    autocomplete(state) {
      return state.autocomplete;
    }
  }
};
