import Vue from "vue";
import Vuex from "vuex";

import map from "./map";
import parking from "./parking";
import autocomplete from './autocomplete';
import ad from './ad';
import notification from './notification';
import subscriptions from './subscription';

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules:{
    map,
    parking,
    autocomplete,
    ad,
    notification,
    subscriptions
  }
});

