import Axios from "axios";

export default {
  state: {
    mapLocation: "",
    mapPosition: { latitude: 0, longitude: 0 },
    mapMarkers: [],
    googleAPIKey: "AIzaSyDafpM70iDVXjbmLIML4rrryXtocCa286k"
  },
  mutations: {
    setMapLocation(state, payload) {
      state.mapLocation = payload;
    },
    setMapPosition(state, payload) {
      state.mapPosition = payload;
    },
    setMapMarkers(state, payload){
      state.mapMarkers = payload;
    }
  },
  actions: {
    loadLocation(state, payload) {
      var self = this;
      var urlBase = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
      var url =
        urlBase +
        payload.latitude +
        "," +
        payload.longitude +
        "&key=" +
        state.getters.googleAPIKey;

      Axios.get(url).then(response => {
        if (response == null || response == undefined || response == "") return;
        if (response.data.results.length <= 0) return;

        var address_components = response.data.results[0].address_components;
        address_components.forEach(function(element) {
          if (element.types.includes("locality")) {
            state.commit("setMapLocation", element.long_name);
          }
        }, this);
      });
    },
    loadGeolocation(state) {
      let coords = {
        latitude: 57.04178,
        longitude: 9.918808
      };
      let locationServices = navigator.geolocation;
      if(navigator.geolocation) {
        locationServices.getCurrentPosition((position) => {
          let realCoords = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          };
        });
      }
      state.commit("setMapPosition", coords);
      state.dispatch("loadLocation", coords);
    },
    loadMapMarkers({commit}, payload){

      var markers = [];

      payload.forEach(function(parkingSpace) {

        var lat = parseFloat(parkingSpace.latitude);
        var lng = parseFloat(parkingSpace.longitude);
        var id = parkingSpace.id;

        markers.push({id:id, position: {lat: lat, lng: lng}});
      }, this);

      commit('setMapMarkers', markers);
    },
    findGeolocation(state, payload) {

      var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + payload;
      Axios.get(url)
        .then(response => {
          if(response == null || response.status != 200 || response.data.results.length <= 0) return;

          var locaction = response.data.results[0].geometry.location;
          var position = {
            latitude: locaction.lat,
            longitude: locaction.lng
          };
          state.commit('setMapPosition', position);
        })
        .catch(err => {
          console.log(err);
        });
    }
  },
  getters: {
    mapLocation(state) {
      return state.mapLocation;
    },
    mapPosition(state) {
      return state.mapPosition;
    },
    mapMarkers(state){
      return state.mapMarkers;
    },
    googleAPIKey(state) {
      return state.googleAPIKey;
    }
  }
};
