import Axios from "axios";
import Guid from "./../../helpers/guid";

export default {
    state:{
        notifications:[],
        activeNotifications:[]
    },
    mutations:{
        setNotifications(state, payload){
            state.notifications = payload;
        },
        addNotification(state, payload){
            state.notifications.push(payload);
        },
        setActiveNotifications(state, payload){
            state.activeNotifications = payload;
        },
        addActiveNotification(state, payload){
            state.activeNotifications.push(payload);
        },
        removeActiveNotification(state, payload){
            state.activeNotifications.splice(index, 1);
        }
    },
    actions:{
        createActiveNotifications(state, payload){
           
            state.commit("addActiveNotification", payload);
            setTimeout(function(){ 
                state.dispatch("deleteActiveNotification", payload.id);
            }, 5000);
        },
        deleteActiveNotification(state, payload){

            let notifications = state.getters.activeNotifications;
            let index = notifications.findIndex(function(element){
                return element.id == payload;
            });

            notifications.splice(index, 1);

            state.commit("setActiveNotifications", notifications);
        }
    },
    getters:{
        notifications(state){
            return state.notifications;
        },
        activeNotifications(state){
            return state.activeNotifications;
        },
        parkingSubscribtionObj(state){
            return parkingPlace => {
                let id = Guid.genereteGUID();                
                let title = 'New Subscription';
                let text = 'You have subscribed to '+ parkingPlace.name +' Parking lot.';
                let parkingPlaceId = parkingPlace.id;
                
                return {id, parkingPlaceId, title, text};
            }
        },
        parkingClosedSubscribtionObj(state){
            return parkingPlace => {
                let id = Guid.genereteGUID();
                let title = 'Closed Subscription';
                let text = 'You are no longer subscribed to '+ parkingPlace.name +' Parking lot.';
                let parkingPlaceId = parkingPlace.id;
                
                return {id, parkingPlaceId, title, text};
            }
        }
    }
}