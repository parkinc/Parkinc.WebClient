import ParkingPlaces from "./parkingPlaces.js";
import Vue from 'vue';

export default {
  state: {
    parkingPlaces: [],
    parkingHeaders: [
      { text: "Name", align: "left" },
      { text: "Open", align: "center" },
      { text: "Payment", align: "center" },
      { text: "Status", align: "center" },
      { text: "Longitude", align: "center" },
      { text: "Latitude", align: "center" },
      { text: "All", align: "center" },
      { text: "Free", align: "center" }
    ],
    activeParkingPlace: {}
  },
  mutations: {
    SOCKET_CONNECT(state) {
      console.log('SOCKET_CONNECT');
    },
    SOCKET_DISCONNECT(state) {
      console.log('SOCKET_DISCONNECT');
    },
    setParkingPlaces(state, payload) {
      state.parkingPlaces = payload;
    },
    setActiveParkingPlace(state, payload) {
      state.activeParkingPlace = payload;
    }
  },
  actions: {
    socket_parkingData({ commit }, message) {
      const parkingPlaces = message._parkingData.map((place, idx) => {
        
        let internalPlace = {}
        for(let propKey in place) {
          if(place.hasOwnProperty(propKey)) {
            let newPropKey = propKey.substr(1);
            internalPlace[newPropKey] = place[propKey];
          }
        }
        if(!place.hasOwnProperty("id")) {
          internalPlace["id"] = idx + 1;
        }
        return internalPlace
      });
      commit('setParkingPlaces', parkingPlaces);
    },
    loadParkingPlaces({ io, commit, getters }, payload) {
      (new Vue()).$socket.emit('parkingDataRequest', {});
    },
    setActiveParkingPlaceWithId({ commit, getters }, payload) {
      var activeParkingPlace = getters.parkingPlaces.find(function(element) {
        return element.id == payload;
      });

      if (activeParkingPlace == undefined) return;
      commit("setActiveParkingPlace", activeParkingPlace);
    }
  },
  getters: {
    parkingPlaces(state) {
      return state.parkingPlaces;
    },
    parkingHeaders(state) {
      return state.parkingHeaders;
    },
    activeParkingPlace(state) {
      return state.activeParkingPlace;
    },
    getParkingPlaceWithId(state) {
      return id => state.parkingPlaces.find(function(element) {
        return element.id == id;
      });
    }
  }
};
