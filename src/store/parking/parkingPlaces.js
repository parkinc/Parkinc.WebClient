export default{

    getPlaces: function(){

        return [
            {
              id:100,
              name: "Budolfi Plads",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "0",
              longitude: "9.9183",
              latitude: "57.0475",
              max_count: "0",
              free_count: "0"
            },
            {
              id:101,              
              name: "C W Obel",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.910837",
              latitude: "57.052425",
              max_count: "376",
              free_count: "376"
            },
            {
              id:102,              
              name: "Friis",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.927327",
              latitude: "57.047688",
              max_count: "731",
              free_count: "503"
            },
            {
              id:103,              
              name: "Føtex",
              is_open: "0",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.924956",
              latitude: "57.04754",
              max_count: "150",
              free_count: "128"
            },
            {
              id:104,              
              name: "Gåsepigen",
              is_open: "0",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.91376",
              latitude: "57.04751",
              max_count: "145",
              free_count: "0"
            },
            {
              id:105,              
              name: "Kennedy Arkaden",
              is_open: "0",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.918808",
              latitude: "57.04178",
              max_count: "350",
              free_count: "188"
            },
            {
              id:106,              
              name: "Kongrescenter",
              is_open: "0",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.91458",
              latitude: "57.044556",
              max_count: "624",
              free_count: "558"
            },
            {
              id:107,              
              name: "Musikkens Hus",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.93311",
              latitude: "57.047367",
              max_count: "285",
              free_count: "258"
            },
            {
              id:108,              
              name: "Palads",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "0",
              longitude: "9.920601",
              latitude: "57.050137",
              max_count: "475",
              free_count: "319"
            },
            {
              id:109,              
              name: "Salling",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.922735",
              latitude: "57.04758",
              max_count: "312",
              free_count: "205"
            },
            {
              id:110,              
              name: "Sauers Plads",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.92745",
              latitude: "57.044193",
              max_count: "360",
              free_count: "126"
            },
            {
              id:111,              
              name: "Sømandshjemmet",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "1",
              longitude: "9.93556",
              latitude: "57.04646",
              max_count: "78",
              free_count: "26"
            },
            {
              id:112,              
              name: "Toldbodgade",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "0",
              longitude: "9.917886",
              latitude: "57.052143",
              max_count: "0",
              free_count: "0"
            },
            {
              id:113,              
              name: "Østre Havn",
              is_open: "1",
              is_payment_active: "0",
              status_park_place: "0",
              longitude: "9.935964",
              latitude: "57.048183",
              max_count: "0",
              free_count: "0"
            }
          ];
    }
}