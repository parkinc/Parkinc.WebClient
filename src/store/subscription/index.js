import Axios from "axios";

export default {
  state: {
    subscriptions: []
  },
  mutations: {
    setSubscriptions(state, payload) {
      state.subscriptions = payload;
    },
    addSubscription(state, payload) {
      state.subscriptions.push(payload);
    },
    removeSubscription(state, payload) {
        
        let data = state.subscriptions;
        
        for(var i = 0; i < data.length; i++) {
            if(data[i].id == payload) {
                data.splice(i, 1);
                break;
            }
        }
    }
  },
  actions: {
    loadSubscriptions(state) {
      var url =
        //"http://localhost:52364/api/subscriptions?email=miropakanec@gmail.com";
        "http://api.parkinc.valorl.com/notifications/api/subscriptions?email=miropakanec@gmail.com";
      Axios.get(url).then(response => {
        state.commit("setSubscriptions", response.data);
      });
    },
    subscribe(state, payload) {
      var url = "http://api.parkinc.valorl.com/notifications/api/subscriptions";
      var subscription = {
        ParkingPlaceId: payload.parkingPlaceId,
        Email: payload.email,
        Phone: payload.phone,
        Hour: payload.hour,
        Minute: payload.minute
      };

      Axios.post(url, subscription)
        .then(response => {
          state.commit("addSubscription", response.data);
        })
        .catch(err => {
          throw err;
        });
    },
    unsubscribe(state, payload) {
      let subscription = state.getters.subscriptions.find(function(element) {
        return element.parkingPlaceId == payload;
      });

      var url = "http://api.parkinc.valorl.com/notifications/api/subscriptions/" + subscription.id;

      Axios.delete(url)
        .then(response => {
          if (response.status == 200) {
            state.commit("removeSubscription", subscription.id);
          }
        })
        .catch(err => {
          throw err;
        });
    }
  },
  getters: {
    subscriptions(state) {
      return state.subscriptions;
    }
  }
};
